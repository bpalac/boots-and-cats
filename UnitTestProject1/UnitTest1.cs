﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BootsAndCats2;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Validate_the_cats_name()
        {
            var bootsAndCats = new BootsAndCats();
            string name = "Imaginary Friend";
            bootsAndCats.nameCat(name);
            Assert.AreEqual(name, bootsAndCats.CatName);
        }
    }
}
